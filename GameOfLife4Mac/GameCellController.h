/**
 GameCellController.h
 
 The game controller for the game of life. Tracks and maintains cells and board state 
 throughout the game. Receives clicks from the view to populate new cells.
 
 @copyright Copyright (c) 2011 HomeForDerangedScientists. All rights reserved.
 @author Seth Landsman <mailto:seth@homeforderangedscientists.net>
 */

#import <Foundation/Foundation.h>
#import <GraphPaperFramework/GraphPaperFramework.h>

@interface GameCellController : NSObject <CellSource>

/** tracks the game generation (how many turns have been executed) */
@property (assign) int generation;

/**
 Executes a single round of execution
 */
-(void)executeOneStep;

/**
 @return the number of cells currently active on the board
 */
-(int)cellCount;

/**
 Resets the board to have no cells and at the first generation
 */
-(void)resetBoard;

/**
 Populate the board with an array of cells
 */
-(void)addCells:(NSArray *)cells;

/**
 Populate the board with cells

 TODO Not yet implemented
 
 @param cells the number of cells to create
 @param size the dimensions of the active board to populate
 */
-(void)randomizeBoardWithCells:(int)cells andGridSize:(NSSize)size;

@end
