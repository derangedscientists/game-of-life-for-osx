/**
 GOLDocument.m
 
 An instance of the GOL ecosystem, including the UI components, document persistence, and 
 game controller
 
 @copyright Copyright (c) 2013 HomeForDerangedScientists. All rights reserved.
 @author Seth Landsman <mailto:seth@homeforderangedscientists.net>
 */

#import "GOLDocument.h"
#import "GameCellController.h"

@interface GOLDocument(private) 

/**
 Private method to update the status field
 */
-(void)doUpdateStatus;

@end

@implementation GOLDocument
{
    /** timer to control the play loop */
    NSTimer *play_timer;
    /** the game controller that manages all cells */
    GameCellController *controller;
    /** local cache of the number of cells to be in a random board */
    int number_of_cells;
    /** local cache of the sparseness of a board for a random board*/
    float sparseness;
    /** local cache of the size of the board for a random board */
    NSSize board_size;
}

#pragma mark - Properties

@synthesize toolbar;
@synthesize view;
@synthesize status;
@synthesize currentState;
@synthesize configPanel;
@synthesize numberOfCellsField;
@synthesize sparenessField;
@synthesize widthField;
@synthesize heightField;

#pragma mark - Static properties

static NSString *PLAY_STATE = @"Playing";
static NSString *STOP_STATE = @"Stopped";

#pragma mark - Initializer methods

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Document specific methods

- (NSString *)windowNibName
{
    return @"GOLDocument";
}

/**
 Called on loading of the window
 
 <ol>
    <li>Once loaded, create the initial board of a fixed size (50, 50) </li>
    <li>initialize fields related to random generation (a lot of TODO here)</li>
    <li>initialize the default state information, including setting the default STOP play state and the controller</li>
    <li>setup connections to the notification center</li>
    <li>setup the controller with the cells and generation buffer, which can't be done until the controller exists</li>
 </ol>
 */
- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    
    board_size = NSMakeSize((int)50, (int)50);
    
    /* related to randomization default params ... TODO */
    sparseness = [sparenessField intValue];
    number_of_cells = (int)(board_size.height * board_size.width * sparseness * .01);
    [widthField setStringValue:[NSString stringWithFormat:@"%i", (int)board_size.width]];
    [heightField setStringValue:[NSString stringWithFormat:@"%i", (int)board_size.height]];
    [numberOfCellsField setStringValue:[NSString stringWithFormat:@"%i", number_of_cells]];
    
    /* init the controller and default play state */
    currentState = STOP_STATE;

    if (controller == nil)
    {
        controller = [[GameCellController alloc] init];
    }
    view.source = controller;
        
    /* init the notification center */
    NSNotificationCenter *notctr = [NSNotificationCenter defaultCenter];
    
    [notctr addObserver:self selector:@selector(handleUpdate:) 
                   name:[CellSourceConstants kGPVCellsDidUpdate] object:nil];
    [notctr addObserver:controller selector:@selector(handleClick:) 
                   name:[ClickSourceConstants kGPVClickDidOccur] object:view];
    
    [self doUpdateStatus];
}

#pragma mark - Data read and write methods

/**
 Write the serialized property list for storing document state
 
 Document state currently consists of the dictionary of cells and the generation
 
 */
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    NSString *error;
    
    NSMutableDictionary *rootObj = [NSMutableDictionary dictionary];
    
    NSSet *c = [controller cells];
    NSEnumerator *en = [c objectEnumerator];
    GraphPaperCell *cell;
    NSMutableArray *a = [NSMutableArray array];
    while (cell = [en nextObject]) 
    {
        NSMutableDictionary *d = [NSMutableDictionary dictionary];
        [d setObject:[NSNumber numberWithInt:cell.x] forKey:@"x"];
        [d setObject:[NSNumber numberWithInt:cell.y] forKey:@"y"];
        [a addObject:d];
    }
    [rootObj setObject:a forKey:@"cells"];
    NSNumber *n = [NSNumber numberWithInt:[controller generation]];
    [rootObj setObject:[n stringValue] forKey:@"generation"];
    
    NSData *xmlData;
    xmlData = [NSPropertyListSerialization dataFromPropertyList:rootObj
                                               format:NSPropertyListXMLFormat_v1_0 
                                               errorDescription:&error];
    return xmlData;
}

/**
 Read in the array of cells and generation from the stored document
 
 Set both the buffer and set the controller directly. The reason is that
 the controller might not exist yet.
 
 */
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    NSArray *cell_restore_buffer;
    int generation_buffer;

    NSString *error;
    NSDictionary *plist = [NSPropertyListSerialization propertyListFromData:data
                                mutabilityOption:NSPropertyListImmutable
                                format:nil
                                errorDescription:&error];
    
    NSArray *c = [plist objectForKey:@"cells"];
    NSEnumerator *en = [c objectEnumerator];
    NSDictionary *d;
    NSMutableArray *array = [NSMutableArray array];
    
    while (d = [en nextObject]) {
        NSNumber *n = [d objectForKey:@"x"];
        int x = [n intValue];
        n = [d objectForKey:@"y"];
        int y = [n intValue];
        GraphPaperCell *c = [[GraphPaperCell alloc] initWithX:x andY:y];
        [array addObject:c];
    }
    cell_restore_buffer = array;
    NSString *tn = [plist objectForKey:@"generation"];
    generation_buffer = [tn intValue];
        
    if (controller == nil)
    {
        controller = [[GameCellController alloc] init];
    }
    
    [controller resetBoard];
    [controller addCells:cell_restore_buffer];
    controller.generation = generation_buffer;
    
    [self doUpdateStatus];
    
    return YES;
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

#pragma mark - Notification management

-(void)doUpdateStatus
{
    NSString *state = self.currentState;
    NSString *generation = [NSString stringWithFormat:@"%i", [controller generation]];
    NSString *cells = [NSString stringWithFormat:@"%i", [controller cellCount]];
    
    NSString *statusValue = [[NSString alloc] initWithFormat:@"State: %@, generation: %@, cell count: %@", 
                             state, generation, cells];
    
    [status setStringValue:statusValue];
}

-(void)handleUpdate:(id)source
{
    [self doUpdateStatus];
}

#pragma mark - Toolbar and UI methods

-(IBAction)step:(id)sender
{
    [controller executeOneStep];
}

- (void)executePlayLoop:(NSTimer*)theTimer
{
    [self step:self];
}

-(IBAction)play:(id)sender
{
    self.currentState = PLAY_STATE;
    if (play_timer == nil) 
    {
        play_timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                    selector:@selector(executePlayLoop:) userInfo:nil repeats:YES];
    }
}

-(IBAction)reset:(id)sender
{
    [self stop:self];
    [controller resetBoard];
}

-(IBAction)stop:(id)sender
{
    self.currentState = @"Stopped";
    [play_timer invalidate];
    play_timer = nil;
    [self doUpdateStatus];
}

-(IBAction)configure:(id)sender
{
    [configPanel makeKeyAndOrderFront:nil];
}

-(IBAction)randomizeBoard:(id)sender
{
    [controller randomizeBoardWithCells:number_of_cells andGridSize:board_size];
}

#pragma mark - UI callbacks

-(IBAction)slidersMoved:(id)sender 
{
    sparseness = [sparenessField intValue];    
    number_of_cells = (int)(board_size.height * board_size.width * sparseness * .01);
    [numberOfCellsField setStringValue:[NSString stringWithFormat:@"%i", number_of_cells]];
}

-(IBAction)heightChanged:(id)sender
{
    board_size.height = [[heightField stringValue] intValue];
    number_of_cells = (int)(board_size.height * board_size.width * sparseness * .01);
    [numberOfCellsField setStringValue:[NSString stringWithFormat:@"%i", number_of_cells]];
}

-(IBAction)widthChanged:(id)sender
{
    board_size.width = [[widthField stringValue] intValue];
    number_of_cells = (int)(board_size.height * board_size.width * sparseness * .01
                            );
    [numberOfCellsField setStringValue:[NSString stringWithFormat:@"%i", number_of_cells]];
}

#pragma mark - Clean up and deallocation

-(void)dealloc
{
    [self shutdownActivity];
}

-(void)close
{
    [self shutdownActivity];
    [super close];
}

-(void)shutdownActivity
{
    [self stop:self];
    NSNotificationCenter *notctr = [NSNotificationCenter defaultCenter];
    [notctr removeObserver:self];
    [notctr removeObserver:view];
}

-(BOOL)hasUnautosavedChanges
{
    return YES;
}

-(BOOL)isDocumentEdited
{
    return YES;
}

@end
