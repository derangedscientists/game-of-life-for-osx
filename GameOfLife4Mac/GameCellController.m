/**
 GameCellController.m
 
 The game controller for the game of life. Tracks and maintains cells and board state 
 throughout the game. Receives clicks from the view to populate new cells.
 
 @copyright Copyright (c) 2011 HomeForDerangedScientists. All rights reserved.
 @author Seth Landsman <mailto:seth@homeforderangedscientists.net>
 */

#import "GameCellController.h"
#import <TwoKeyDictionaryFramework/TwoKeyMutableDictionary.h>

@interface GameCellController(private) 
/**
 Add a cell to the game
 
 Fails silently if the cell already exist

 @param cell the cell to add
 */
-(void)addCell:(GraphPaperCell *)cell;

/**
 Add a cell a specified x,y coordinate to the game
 
 Fails silently if the cell already exist

 TODO why pass in a cell? Just create it on the fly
 
 @param cell the cell to add
 @param x the x coordinate
 @param y the y coordinate
 */
-(void)addCell:(GraphPaperCell *)cell withX:(int)x andY:(int)y;

/**
 Add a cell to the specified dictionary
 
 Fails silently if the cell already exist

 @param cell the cell to add
*/
-(void)addCell:(GraphPaperCell *)c toDictionary:(TwoKeyMutableDictionary *)dict;

/**
 Delete a cell from the game

 Fails silently if the cell does not exist

 @param cell to delete
 */
-(void)deleteCell:(GraphPaperCell *)cell;

/**
 Delete a cell from the specified x,y coordinate

 Fails silently if the cell does not exist
 
 @param x the x coordinate
 @param y the y coordinate
 */
-(void)deleteCellWithX:(int)x andY:(int)y;

/**
    Checks for existence of a cell at a specified location

 @param x the x coordinate
 @param y the y coordinate
 @return TRUE if a cell exits at the specified x, y coordinates
 */
-(BOOL)hasCellWithX:(int)x andY:(int)y;

/**
 Send out a notification via NSNotificationCenter that the set of cells has changed
 */
-(void)notifySinks;

/**
 Return the number of cells surrounding the specified x, y coordinates

 @param x the x coordinate
 @param y the y coordinate
*/
-(int)getCellCountForX:(int)x andY:(int)y;
@end

@implementation GameCellController {
    TwoKeyMutableDictionary *cells; /** the internal collections of cells */
    int dimension_max; /** the maximum x or y coordinate that has a cell, used for determining the  board size to check */
}

#pragma mark - Properties

@synthesize generation;

#pragma mark - Initializers

-(GameCellController *)init
{
    self = [super init];
    if (self)
    {
        dimension_max = 0;
        generation = 0;
        cells = [[TwoKeyMutableDictionary alloc] init];
    }
    return self;
}

#pragma mark - Board introspection

-(int)cellCount 
{
    return (int)[[cells allValues] count];
}

-(NSSet *)cells {
    if (cells != nil) {
        return [[NSSet alloc] initWithArray:[cells allValues]];    
    }
//    NSLog(@"Number of cells is %lui", [cells count]);
    return [[NSSet alloc] init];
}

-(BOOL)hasCellWithX:(int)x andY:(int)y
{
    GraphPaperCell *cell = [cells objectForKeyOne:[NSNumber numberWithInt:x] 
                                        andKeyTwo:[NSNumber numberWithInt:y]];
    if (cell != nil) {
        return YES;
    } else {
        return NO;
    }
}

-(int)getCellCountForX:(int)x andY:(int)y
{
    int count = 0;
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x-1] 
                     andKeyTwo:[NSNumber numberWithInt:y-1]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x-1] 
                     andKeyTwo:[NSNumber numberWithInt:y]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x-1] 
                     andKeyTwo:[NSNumber numberWithInt:y+1]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x] 
                     andKeyTwo:[NSNumber numberWithInt:y-1]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x] 
                     andKeyTwo:[NSNumber numberWithInt:y+1]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x+1] 
                     andKeyTwo:[NSNumber numberWithInt:y-1]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x+1] 
                     andKeyTwo:[NSNumber numberWithInt:y]] != nil) 
    {
        count++;
    }
    
    if ([cells objectForKeyOne:[NSNumber numberWithInt:x+1] 
                     andKeyTwo:[NSNumber numberWithInt:y+1]] != nil) 
    {
        count++;
    }
    
    return count;
}

#pragma mark - Board manipulation

-(void)addCell:(GraphPaperCell *)cell 
{
    [self addCell:cell withX:cell.x andY:cell.y];
}

-(void)deleteCell:(GraphPaperCell *)cell
{
    [self deleteCellWithX:cell.x andY:cell.y];
}

-(void)addCell:(GraphPaperCell *)cell withX:(int)x andY:(int)y
{
    [cells setObject:cell forKeyOne:[NSNumber numberWithInt:x] andKeyTwo:
     [NSNumber numberWithInt:y]];
    if (x > dimension_max) {
        dimension_max = x;
    }
    if (y > dimension_max) {
        dimension_max = y;
    }
    [self notifySinks];    
}

-(void)deleteCellWithX:(int)x andY:(int)y
{
    [cells removeObjectForKeyOne:[NSNumber numberWithInt:x] andKeyTwo:
     [NSNumber numberWithInt:y]];
    [self notifySinks];    
}

-(void)addCell:(GraphPaperCell *)c toDictionary:(TwoKeyMutableDictionary *)dict
{
    [dict setObject:c forKeyOne:[NSNumber numberWithInt:c.x] andKeyTwo:[NSNumber numberWithInt:c.y]];
    if (c.x > dimension_max) {
        dimension_max = c.x;
    }
    if (c.y > dimension_max) {
        dimension_max = c.y;
    } 
}

-(void)resetBoard
{
    [cells removeAllObjects];
    generation = 0;
    dimension_max = 0;
    [self notifySinks];
}

-(void)randomizeBoardWithCells:(int)cells andGridSize:(NSSize)size
{
    NSLog(@"Randomizing ...");
}

-(void)addCells:(NSArray *)c
{
    NSEnumerator *en = [c objectEnumerator];
    GraphPaperCell *cell;
    while (cell = [en nextObject])
    {
        [self addCell:cell];
    }
}

#pragma mark - Notification management

-(void)notifySinks
{
    NSNotification *not = [NSNotification 
                           notificationWithName:[CellSourceConstants kGPVCellsDidUpdate] object:self];
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    [ctr postNotification:not];
}

-(void)handleClick:(NSNotification *)not
{
//    NSLog(@"Controller handling click ...");
    NSValue *v = [[not userInfo] objectForKey:@"point"];
    NSPoint loc = [v pointValue];
//    NSLog(@"Got a click at %f, %f", loc.x, loc.y);
    GraphPaperCell *cell = [[GraphPaperCell alloc] initWithPoint:loc];
    if ([self hasCellWithX:loc.x andY:loc.y]) {
        [self deleteCellWithX:loc.x andY:loc.y];
    } else {
        [self addCell:cell withX:cell.x andY:cell.y];
    }
}

#pragma mark - Game functions

-(void)executeOneStep {
//    NSLog(@"Executing a single step.");
    TwoKeyMutableDictionary *new_cells = [[TwoKeyMutableDictionary alloc] init];    
    int new_dimension_max = 0;

    for (int x=0; x<=dimension_max + 1; x++) {
        for (int y=0; y<=dimension_max + 1; y++) {
            
            int count = [self getCellCountForX:x andY:y];
            if (count < 2) {
                
            } else if (count == 2) {
                if ([cells objectForKeyOne:[NSNumber numberWithInt:x] 
                                 andKeyTwo:[NSNumber numberWithInt:y]] != nil) {
                    [self addCell:[[GraphPaperCell alloc] initWithX:x andY: y] toDictionary:new_cells];
                    if (x > new_dimension_max) {
                        new_dimension_max = x;
                    }
                    if (y > new_dimension_max) {
                        new_dimension_max = y;
                    }
                }
            } else if (count == 3) {
                [self addCell:[[GraphPaperCell alloc] initWithX:x andY: y] toDictionary:new_cells];
                if (x > new_dimension_max) {
                    new_dimension_max = x;
                }
                if (y > new_dimension_max) {
                    new_dimension_max = y;
                }
            }             
        }
    }
    cells = new_cells;
    generation++;
    dimension_max = new_dimension_max;
    [self notifySinks];
}

@end
