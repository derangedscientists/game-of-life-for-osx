/**
 GOLDocument.h
 
 An instance of the GOL ecosystem, including the UI components, document persistence, and 
 game controller
 
 @copyright Copyright (c) 2011 HomeForDerangedScientists. All rights reserved.
 @author Seth Landsman <mailto:seth@homeforderangedscientists.net>
 */

#import <Cocoa/Cocoa.h>
#import <GraphPaperFramework/GraphPaperFramework.h>

@interface GOLDocument : NSDocument

/** Toolbar associated with this UI instance */
@property (assign) IBOutlet NSToolbar *toolbar;
/** GPV associated with this UI instance */
@property (assign) IBOutlet GraphPaperView *view;
/** Status text field associated with this UI instance */
@property (assign) IBOutlet NSTextField *status;
/** 
 Current state, displayed by the status text field, associated with this UI instance 

 TODO - should be private?
 */
@property (assign) NSString *currentState;
/** Configuration panel associated with this UI instance */
@property (assign) IBOutlet NSPanel *configPanel;
/** Number of cells to display in a random board, associated with this UI instance */
@property (assign) IBOutlet NSTextField *numberOfCellsField;
/** How dense to make the random board, associated with this UI instance */
@property (assign) IBOutlet NSSlider *sparenessField;
/** Height of the random board, associated with this UI instance */
@property (assign) IBOutlet NSTextField *heightField;
/** Width of the random board, associated with this UI instance */
@property (assign) IBOutlet NSTextField *widthField;

/**
 Execute a single game step 
 */
-(IBAction)step:(id)sender;
/**
 Start a run loop, executing a step periodically
 */
-(IBAction)play:(id)sender;
/**
 Reset the board to empty
 */
-(IBAction)reset:(id)sender;
/**
 Step the run loop
 */
-(IBAction)stop:(id)sender;
/**
 Display the configuration panel 
 
 TODO not implemented yet
 */
-(IBAction)configure:(id)sender;
/**
 Randomize the board
 
 TODO not implemented yet
 */
-(IBAction)randomizeBoard:(id)sender;
/**
 Call back for when the configuration panel fields changes
 */
-(IBAction)slidersMoved:(id)sender;
/**
 Call back for when the height field changes
 */
-(IBAction)heightChanged:(id)sender;
/**
 Call back for when the width field changes
 */
-(IBAction)widthChanged:(id)sender;

@end
