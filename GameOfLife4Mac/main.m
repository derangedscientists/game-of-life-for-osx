//
//  main.m
//  GameOfLife4Mac
//
//  Created by Seth Landsman on 12/26/11.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
